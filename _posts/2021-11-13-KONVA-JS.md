---
layout: post
title: KONVA JS
categories: [JavaScript, Konva]
---

I found a front end canvas library call [Konva](https://github.com/konvajs/konva) which is very cool and easy to use.

The example below show that some basic usage about Konva. Try click, drag and transform those shapes.

<iframe height="400" style="width: 100%;" scrolling="no" title="Untitled" src="https://codepen.io/terryFung/embed/yLoRBeQ?default-tab=html%2Cresult" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/terryFung/pen/yLoRBeQ">
  Untitled</a> by terryfung (<a href="https://codepen.io/terryFung">@terryFung</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

