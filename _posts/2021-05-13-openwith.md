---
layout: post
title: emacs openwith
date: 2021-05-13
last_modified_at: 2021-05-13
categories: [emacs]
---

Emacs 雖然可以瀏覽圖片，甚至還可以打開 pdf。 但自己還是覺得用其他相關的軟件打開這些格式文件比較好。

於是發現了 [openwith](https://github.com/thisirs/openwith), 它可以讓 emacs 用其他文件打開相關文件。

Config: 

```lisp
(use-package! openwith
  :after-call (pre-command-hook after-find-file dired-before-readin-hook)
  :config
  (openwith-mode 1))
(after! openwith
  (setq openwith-associations
        (list
         (list (openwith-make-extension-regexp
                '("mpg" "mpeg" "mp3" "mp4"
                  "avi" "wmv" "wav" "mov" "flv"
                  "ogm" "ogg" "mkv"))
               "vlc"
               '(file))
         (list (openwith-make-extension-regexp
                '("png" "gif" "jpeg" "jpg"))
               "feh"
               '(file))
         (list (openwith-make-extension-regexp
                '("pdf"))
               "evince"
               '(file))))
```
如果直接配置 openwith， 在 doom emacs 這裏會出現被 dired mode 覆蓋的問題，後來解決辦法是用 `after-call` 把它排在 dired mode 後面

除了上面的文件，打開 doc, xls 等都可以

![](https://ucarecdn.com/7332dd72-7932-4d5f-a4ac-65d04ca7c31c/Peek202105131406.gif)


