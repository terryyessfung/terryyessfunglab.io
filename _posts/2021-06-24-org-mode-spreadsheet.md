---
layout: post
title: The Spreadsheet in org mode
date: 2021-06-24
last_modified_at: 2021-06-24
categories: [emacs]
---

# Intro

最近想管理下我糟糕的日常財務狀況，於是開始尋找軟件。剛開始找到 ledger， 但是個人試用下後發現有點麻煩,因爲個人需求只是想簡單快速記錄下每天每次的支出和收入,而不想學額外的經濟學和 syntax, command。像是 Excel 那樣就好，帳目一目了然，但缺點是 windows 系，嚴重依賴鼠標，而且啓動起來慢 ><

在尋找解決方案中找到 Richard Stallman 照片(原諒我照片沒了QAQ),忽然靈光一閃 -- Emacs！這個操作系統（誤）有 org mode, 而 org mode 裏有 spreadsheet, spreadsheet 能像是 Execl 一樣輸入公式來計算 fields！ That is！

# My case
多虧 org 官網上非常清晰的 Document[^1], 和 Wong's 大神的這篇 blog 文章[^2], 讓我很快上手這個 spreadsheet。

這是我現在日常在用的其中一個 spreadsheet 作爲例子:
```
| Date       | Event       | type   | Income |  Paid |
|------------+-------------+--------+--------+-------|
| 2021-06-23 | Get money   | income |    500 |       |
|            | MacDonald's | eat    |        |    45 |
|------------+-------------+--------+--------+-------|
| 2021-06-24 | breakfast   | eat    |        |    40 |
|            | Supermarket | eat    |        |  86.6 |
|            | lunch       | eat    |        |    24 |
|            | octopus     | other  |        |   100 |
|------------+-------------+--------+--------+-------|
| Today      |             |        |      0 | 250.6 |
| Total      |             |        |    500 | 295.6 |
| Result     |             |        |        | 204.4 |
#+TBLFM: @>>$4=vsum(@2..@-2)::@>>$5=vsum(@2..-2)::@>$5=(@>>$>> - @>>$>)::@>>>$4=vsum(@-II..@-I)::@>>>$5=vsum(@-II..@-I)
```
一個非常簡單的記錄日常生活收入和支出的表格。這個表格有5個公式，讓我們拆開來看看。

先看 Total，Total 那一行有兩個公式，分別計算所有收入和所有支出：
```
@>>$4=vsum(@2..@-2)
@>>$5=vsum(@2..-2)
```
這兩個公式都很簡單，只是把當前列的記錄加起來而已。

`@` 符號代表 Row，而 `$` 符號代表 Column。`>` 符號代表從表格底部開始數，例如 `>` 代表表格的最後一行，`>>` 代表最後的第二行以此類推。使用相對坐標像是 `>` 符號而不使用絕對坐標 `@9$4` 好處是表格之後會不斷加入新記錄, 比如 Total 這一行不一定永遠在第9行，但它在這個表格裏永遠都是最後的第二行。

--- 

之後我們看看 Result 的公式：
```
@>$5=(@>>$>> - @>>$>)
```
如官方 Document 所說，org mode 引入了 calc package，其中一個功能就是能讓我們進行日常計算方式即 `10 - 1`, `20 - 12`等，而不是使用 lisp 的 postfix 計算方式: `(* (+ 100 20) 2)`。雖然我們可以用 elisp 來操控表格做出些更 cool 的效果,但我 elisp 目前水平很糟糕就算了:(
這裏的公式其實就是簡單的指把 Totoal Income 的結果 減 Total Paid 的結果。

---

最後就是計算 Today 的 Income 和 Paid：
```
@>>>$4=vsum(@-II..@-I)
@>>>$5=vsum(@-II..@-I)
```
因爲不知道今天會有多少記錄,以及不知道昨天的記錄是多少從而不知道從哪裏開始計算今天的記錄，所以用 `>` 符合也不好用了，後來發現一個簡單的辦法就是我們可以用分割線進行定位, 而 `I` 代表的就是分割線。`I` 代表第一條分割線, `II` 代表第二條，而當前面加上 `-` 這個符號就是反轉了， `-I` 代表最後一條分割線，`-II` 代表最後第二條分割線...etc。就此完成這個公式。

其實 org mode 還提供了其他符號請讀者自行查閱。但由此可見 org mode 將符號發揮的淋漓盡致。

完。

# References:
[^1]: [The spreadsheet - Org mode](https://orgmode.org/org.html#The-Spreadsheet)
[^2]: [Org as a spreadsheet system: a short introduction](https://orgmode.org/worg/org-tutorials/org-spreadsheet-intro.html)
