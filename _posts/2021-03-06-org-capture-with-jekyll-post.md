---
layout: post
title: org capture & jekyll post
date: 2021-03-06 18:54:09
last_modified_at: 2021-03-06 18:54:09 
categories: [emacs]
---

---

# intro

最近把这个 blog 从 hexo 换去 jekyll了，没有额外原因，可能是 ruby 影响了我吧。根据 jekyll 官网创建一份新 post 文章需要手动创建文件，文件名根据以下格式：

```shell
YEAR-MONTH-DAY-title.md
```

另外每份文章都需要在开头插入front matter：

```markdown
---
layout: post
title:  "Welcome to Jekyll!"
---
# Welcome
**Hello world**, this is my first Jekyll blog post.
```

如上所见，每次写新 post 都要重复上面的动作，长时间下显得麻烦。于是想到可以用 emacs + org capture 解决。

---

# Prepare

解决这个问题要三个东西

1. emacs
2.  org-mode
3. Template

---

# Emacs

```lisp
(defun create-blog-post ()
        "Create blog post"
        (interactive)
        (let ((name (read-string "Post title: ")))
          (expand-file-name
           (concat 
            (org-read-date) "-" (format "%s.md" name))
           ;; change your blog directory below
           "~/Documents/myjekyllblog/_posts/")))
```

这个简单的函数能让 emacs 弹出输入框，我们写入文章的标题即可:

![](https://ucarecdn.com/44e4f273-1cd8-4972-bfed-ff99f918c4bd/-/format/auto/-/quality/smart/)

接下来会弹出输入日期的框，这里用了 org mode 的 `org-read-date` ，用户可选择日期，或者按回车键直接创建当日日期，效果如下：

![](https://ucarecdn.com/149547c8-5f39-464e-8810-b4e8f71eea3a/-/format/auto/-/quality/smart/)

---

# Org-capture

org-capture 是 org mode 里很方便的工具之一，能让你在使用 emacs 的期间，随时随地想到什么便写些什么，比如在写代码的时候，脑子里突然想到一款 app 的新 idea，想要立刻记录下来，便可以立刻按 `M-x org-capture`,把想法记录下来 。因为设定了 `org-capture-templates`,所以 emacs 打开了这样的buffer：

![](https://ucarecdn.com/f3bf99d4-9db2-42fb-8e1e-ffd98315b2d1/-/format/auto/-/quality/smart/)

在键盘按下 i, q, p 分别对应不同的模版，这样在记笔记时便可使用预定好的模版，直接输入内容，很适合时间比较紧迫的情况，比如开会，或者上课记笔记时候。

因此，我们也可以用这个排版功能来生成我们 jeklly 需要的 post 模版。

首先，先设定 `org-capture-templates`, 因为我已经设定了一些模版，所以这里我加入新一项：

```lisp
(setq org-capture-templates
        '(("i" "Todo Inbox" entry (file "~/Documents/org/Inbox.org")
           "* %?\n")
          ("q" "Quick Note" entry (file "~/Documents/org/Drafts.org")
           "* %?\n %T\n ")
          ;; insert new capture here
          ("p" "Post" plain
                (file create-blog-post)
                (file "~/.doom.d/post.tmpl"))))
```

因为是 post， 所以取“p“为键值，然后插入之前提及 `create-blog-post` 函数，而最后的 `post.tmpl`  便是我们需要设定的模版文件。

新建一个文件 `post.tmpl` , 其实后缀名叫什么都可以,这里的 `tmpl` 只是让我方便识别是 template 文件而已。文件内容如下：

```
---
layout: post
title: %^{Title}
categories:
---

%?

```

这里放入 jeklly 想要的 Front Matter 便可。

整合流程下来便是：打开 emacs -> `M-x org-capture` -> `p` -> 输入文件名-> 输入日期 -> 输入标题 -> `C-c C-c` ,便在 jekyll 的 `_post` 目录下生成了一份新的 post 文件，之后可以尽情写作了。
