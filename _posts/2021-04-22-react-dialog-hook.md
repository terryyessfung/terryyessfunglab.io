---
layout:  post
title: Create a useDialog hook with Context,Portals
date: 2021-04-22
last_modified_at: 2021-04-22
categories: [react]
---

----
# Table of Contents

1.  [Prepare](#org6bc031b)
2.  [Portal and Dialog component](#org441ebfd)
3.  [Creat a useDialog hook ](#orgb6b0b2c)
4.  [Create a DialogContext ](#org09f5206)
5.  [Start to use~](#orgdea354a)
6.  [Resources](#org556a4bc)

----
<a id="org6bc031b"></a>

# Prepare

-   React
    -   useContext, Provider, Portal
-   Typescript
-   tailwind css, headless ui

---
<a id="org441ebfd"></a>
# Portal and Dialog component

根据官方的 React 文档[^1],

> Portals provide a first-class way to render children into a DOM node that exists
> outside the DOM hierarchy of the parent component.

以我肤浅的理解，Portal 可以帮助我们把某些组件不想受到 App 里面组件的影响，而把它渲染在 App 外。简单来说就是在我们 index.html 里除了已有的 `#root` 外加插一个 dom element 让 react 也渲染它。

那么什么组件适合这样做？

我想到 Dialog 在我的项目里很适合用这个方法（这篇文章就是把这个目前我项目里的 Dialog component提炼并简化了出来）。Dialog 需要弹出在整个 app 之上，是一个 First Class component，所以不应该受到其他组件的影响， 而且它是要重复使用的， 那么实现这个就需要把它 ”Global“ 并集中管理我们的 state。

但首先，我们先来写我们的 dialog component：

```typescript
import { Fragment } from "react";
import { createPortal } from "react-dom";
import { Dialog, Transition } from "@headlessui/react";

const dialogRoot = document.getElementById("dialog-root") as HTMLElement;
export default function MyDoalog() {
  return createPortal(
    <Transition show={true} as={Fragment}>
      <Dialog
        as="div"
        className="fixed inset-0 z-10 overflow-y-auto"
        static
        open={true}
        onClose={() => {}}
      >
        <div className="min-h-screen px-4 text-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className="inline-block h-screen align-middle"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
              <Dialog.Title
                as="h3"
                className="text-lg font-medium leading-6 text-gray-900"
              >
                Payment successful
              </Dialog.Title>
              <div className="mt-2">
                <p className="text-sm text-gray-500">
                  Your payment has been successfully submitted. We’ve sent your
                  an email with all of the details of your order.
                </p>
              </div>

              <div className="mt-4">
                <button
                  type="button"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                  onClick={() => {}}
                >
                  Got it, thanks!
                </button>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition>,
    dialogRoot
  );
}

```

​	`createPortal()` api 需要 两个参数，第一个是我们的 Dialog componet，这里我使用 tailwind css[^2] 并修改了 headless ui[^3]官网里的 dialog 例子。第二个参数是渲染组件到哪个 html element，由于使用了 typescript，我们需要定义 HTMLElement type：

```typescript
const dialogRoot = document.getElementById("dialog-root") as HTMLElement;
```

在 `public/index.html` 里，加入我们的 `#dialog-root` div

```html
<body class="bg-black">
  <noscript>You need to enable JavaScript to run this app.</noscript>
  <div id="dialog-root"></div>
  <div id="root"></div>
</body>
```

效果如下：

![image1](https://ucarecdn.com/24af04fb-712c-4157-ac23-0462cdb10f59/-/format/auto/-/quality/lightest/)

----

<a id="orgb6b0b2c"></a>

# Create a useDialog hook

为什么我们需要自定义一个 hook？:thinking:

根据官方文档[^4]

> Building your own Hooks lets you extract component logic into reusable functions.

如之前提到，我们的 Dialog 同时希望它是能够重复使用的。为了解决这个 duplication 问题， 当在其他不同的组件 call 这个 Dialog 时，希望都是同一个 Dialog 只是里面的内容会有所不同。 即是，所有被 call 的 Dialog 看似不一样，但其实都是同一个组件，也就是说它们有着同一个 state (我们需要些 state 来控制 dialog 的开关和内容)。

肤浅地说就是：我们把 dialog 的 state 统一放在我们自定义的 useDialog hook 里，解决了 state duplicate 问题

芜湖～好像有点囉嗦:sweat_smile:

那首先，我们先定义 Dialog 有什么

```typescript
export interface Dialog {
  title: string;
  description: string;
  onOK: () => void;
}
```

我把这段代码放在 `src/types.ts` 里。

如上所见，在这个例子里面，我们的 Dialog 有 标题，内容，和一个 onOK funciton 用来处理 确认按钮的 onclick 事件。

因为是例子，我把他简化了点，读者可以自行加上更多属性，比如 取消按钮的处理，或者按钮的名称等。

接下来，我们来写自定义的 hook:

```typescript
import React, { useState } from "react";
import { Dialog } from "../types";

const useDialog = () => {
  const [dialogIsActive, setDialogIsActive] = useState(false);
  const [dialogContent, setDialogContent] = useState<Dialog | null>(null);

  const handleDialog = (content?: Dialog) => {
    setDialogIsActive(!dialogIsActive);
    if (content) setDialogContent(content);
  };

  return { dialogIsActive, handleDialog, dialogContent };
};

+export default useDialog;
```

在以上代码里，我们定义了两个 state:

1.  `dialogIsActive` : 用来处理我们 dialog 开关
2.  `dialogContent`: 我们 dialog 的内容和 确认按钮

另外，我们有这个 `handleDialog` 的方法来更新以上的state. 有时候我们并不需要更新内容，只是想把 active state 值反转，比如我们 Dialog onClose 时，把 active = false，这情况下我们并不是想更新内容，所以我们可以把 content 设定为 optional。

----
<a id="org09f5206"></a>

# Create a DialogContext

好吧，Context 又是啥子

> Context provides a way to pass data through the component tree without having to pass props down manually at every level.[^5]

前面我们自定义hook 解决了 state duplication 问题，但我们 Dialog 组件的 duplicate 问题呢? 这就是我们这里打算用 Context 的原因。Context 能把我们自定义的 hook 传下去子组件里， 确保所有子组件用的都是同一个 dialog 的 state，也因此，我们只需要一个 Dialog 放在app 之上。 App 的子组件想 call Dialog 只需要通过 React 提供的 context api 来往回更新我们的 Dialog 即可。

以下是我们的 DialogContext：

```typescript
import React, { createContext } from "react";
import useDialog from "./useDialog";
import MyDialog from "./Dialog";

import { Dialog, DialogProviderValue } from "../types";

type Props = {
  children: React.ReactNode;
};

const initialDialog: DialogProviderValue = {
  dialogIsActive: false,
  handleDialog: () => {},
  dialogContent: {
    title: "Sure?",
    description: "Are you sure?",
    onOK: () => {},
  },
};

const DialogContext = createContext(initialDialog);

const DialogProvider = ({ children }: Props) => {
  const { dialogContent, dialogIsActive, handleDialog } = useDialog();
  return (
    <DialogContext.Provider
      value={{ dialogIsActive, handleDialog, dialogContent }}
    >
      {children}
    </DialogContext.Provider>
  );
};

export { DialogContext, DialogProvider };
```

根据文档，我们可以用 React 提供的 `createContext`  api 来创建我们的 context:

```typescript
const DialogContext = createContext(initialDialog);
```

Context 都有一个 Provider 来负责传递值和监测值的改变和更新，这个值就是从 `value` 这里传递。在这个例子里，我们把上面写的 `useDialog` hook 放入 value 里，让 Provider 监测变化和让子组件获取这些值：

```typescript
type Props = {
  children: React.ReactNode;
};

const DialogProvider = ({ children }: Props) => {
  const { dialogContent, dialogIsActive, handleDialog } = useDialog();
  return (
    <DialogContext.Provider
      value={{ dialogIsActive, handleDialog, dialogContent }}
    >
      {children}
    </DialogContext.Provider>
  );
};
```

在 typescript 里， 当用`createContext()`，我们需要定义一些初始值[^6]，其实也是约束我们 Provider value 的值，以免乱传递，或修改加入其他不相干的值。

所以在 `types.ts`, 我们加入新的 interface 来定义我们 value 的 type：

```typescript
export interface DialogProviderValue {
  dialogIsActive: boolean;
  handleDialog: (context?: Dialog) => void;
  dialogContent: Dialog | null;
}
```

上面可以看到我们自定义了这些初始化值:

```typescript
const initialDialog: DialogProviderValue = {
  dialogIsActive: false,
  handleDialog: () => {},
  dialogContent: {
    title: "Sure?",
    description: "Are you sure?",
    onOK: () => {},
  },
};
```

----
<a id="orgdea354a"></a>

# Start to use it~

在创建完以上组件后，我们可以开始使用了！

首先改一改 `app.tsx`，加入我们的 Provider

```typescript
import React from "react";
import { DialogProvider } from "./components/DialogContext";
import Component1 from "./components/Component1";

function App() {
  return (
    <DialogProvider>
      <Component1 />
    </DialogProvider>
  );
 }
```

这里我还加入了简单的 component1 来测试我们的 dialog，代码如下

```typescript
import React, { useContext } from "react";
import { DialogContext } from "./DialogContext";

export default function Component1() {
  const { dialogIsActive, handleDialog, dialogContent } = useContext(
    DialogContext
  );
  return (
    <div className="flex justify-center items-center h-screen m-auto">
      <button className="bg-blue-800 text-white py-3 px-6 text-2xl font-bold rounded-xl focus:outline-none hover:bg-blue-600 transition ease-out duration-500">
        Click me
      </button>
    </div>
  );
}
```

只是简单的按钮和一些 tailwind css。 我们在后面会更新这个按钮的 onClick()。

但现在我们先来修改下我们之前的 Dialog 组件

首先我们加载 `useContext` api，和我们的 `DialogContext`。

```typescript
import { Fragment, useContext } from "react";
import { DialogContext } from "./DialogContext";
```

有了这两个，我们便能在这个组件里获取到由 Provider value 传入的值。

```typescript
export default function MyDoalog() {
  const { dialogIsActive, dialogContent, handleDialog } = useContext(DialogContext);
    return createPortal(
      //...
```

Headless ui 的 Transition 和 Dialog 组件的 props 刚好就有 `show` 和 `open` props，我们只需要把它们赋予给 `dialogIsActive` ，非常直观:nerd_face:。

如之前提到，`onClose` 只是想转换 active 的值，所以我们不需要传入 content 也可以。

```typescript
return createPortal(
  <Transition show={dialogIsActive} as={Fragment}>
    <Dialog
      as="div"
      className="fixed inset-0 z-10 overflow-y-auto"
      static
      open={dialogIsActive}
      onClose={() => handleDialog()}
    >
```

剩下的就是更改我们 Dialog 的 `title`， `description` 和 `onClick` 方法，这些都在我们 useDialog hook 里的 `dialogContent` 定义了。

```typescript
<div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
  <Dialog.Title
    as="h3"
    className="text-lg font-medium leading-6 text-gray-900"
  >
    {/*change here*/}
    {dialogContent?.title || "Sure?"}
  </Dialog.Title>
  <div className="mt-2">
    <p className="text-sm text-gray-500">
      {/*change here*/}
      {dialogContent?.description || "Are your sure?"}
    </p>
  </div>
  <div className="mt-4">
    <button
      type="button"
      className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
      {/*change here*/}
      onClick={() => dialogContent?.onOK() || {}}
    >
     Confirm
    </button>
  </div>
</div>
```

别忘记我们应该更新 component1 这次我们需要用 handleDialog 里的 content 值了，我们定义一下我们需要的标题和内容，而且更新下 onclick 的方法：

```typescript
import React, { useContext } from "react";
import { DialogContext } from "./DialogContext";

export default function Component1() {
  const { handleDialog } = useContext(DialogContext);

  return (
    <div className="flex justify-center items-center h-screen m-auto">
      <button
        className="bg-blue-800 text-white py-3 px-6 text-2xl font-bold rounded-xl focus:outline-none hover:bg-blue-600 transition ease-out duration-500"
        onClick={() =>
          handleDialog({
            title: "Hi",
            description: "This message is came from component 1",
            onOK: () => {
              console.log("YOOOOO");
            },
          })
        }
      >
        Click me
      </button>
    </div>
  );
}
```

The result:
![](https://ucarecdn.com/b16655a4-50f1-4a1e-80ac-63cb4dadf8b5/Peek202104221507.gif)

我们可以再写另外一个组件并写入不同的 content 来测试 duplication：

```typescript
import React, { useContext } from "react";
import { DialogContext } from "./DialogContext";

export default function Component2() {
  const { dialogIsActive, handleDialog, dialogContent } = useContext(
    DialogContext
  );

  return (
    <button
      className="bg-red-800 text-white py-3 px-6 text-2xl font-bold rounded-xl focus:outline-none hover:bg-red-600 transition ease-out duration-500"
      onClick={() =>
        handleDialog({
          title: "Oh,Hi again",
          description: "This message is came from component 2",
          onOK: () => {
            console.log("Nice to meet you");
          },
        })
      }
    >
      Then Click me
    </button>
  );
}
```

The result:

![](https://ucarecdn.com/a06987dc-f234-47f1-8273-6aff8c70910f/Peek202104221510.gif)



事实上，Alex Suarez[^7] 还提及(在这里也感谢他的 post，让我学习到怎么自定义 hook)，我们的 `handleDialog    `除了放入 object 外，当然还可以放入 component 。 请读者自行发挥～

(完～):kissing_heart:

----

<a id="org556a4bc"></a>


# Resources

[^1]: [React-Portals](https://reactjs.org/docs/portals.html)
[^2]: [tailwind css](https://tailwindcss.com/)
[^3]: [ headless ui dialog](https://headlessui.dev/react/dialog)
[^4]: [Building Your Own Hooks](https://reactjs.org/docs/hooks-custom.html#gatsby-focus-wrapper)
[^5]: [React context](https://reactjs.org/docs/context.html)
[^6]: [Using React hooks in typescript](https://stackoverflow.com/questions/53335907/using-react-context-with-react-hooks-in-typescript)
[^7]: [Your next React Modal with your own "useModal" Hook & Context API ](https://dev.to/alexandprivate/your-next-react-modal-with-your-own-usemodal-hook-context-api-3jg7)

