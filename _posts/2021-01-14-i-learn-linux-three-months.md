---
layout: post
title: i-learn-linux-three-months
date: 2021-01-14 18:54:09
last_modified_at: 2021-01-14 18:54:09 
categories: life
---
![](https://ucarecdn.com/26b55f04-cb0a-4f1f-a691-ff07ad725eb3/-/format/auto/-/quality/smart/)

这是我开始使用linux三个月来，现在桌面的样子。 上面是 polybar，右边是 conky， 下面波浪是 glava。

我用 nitrogen 来管理壁纸，下面这个句代码能更换随机壁纸。
```bash
nitrogen --random --set-scaled /home/YOUR_USER/.wallpapers
```

我写了个很简单的 bash script, 来循环不同系列的主题：
```bash
today_is_even=$(date | awk '{print $3}' | awk '{print $1 % 2}')

if [[ $today_is_even -eq 0 ]]; then
	nitrogen --random --set-scaled /home/terryfung/.wallpapers/dark
else
	nitrogen --random --set-scaled /home/terryfung/.wallpapers/light
fi

```

闲聊下 Arch linux 体验目前感觉非常好,有时会出现一下影响不大的小问题, 暂时没有出现像网上说的会滚挂啥的，但我也会留意去避开这个问题，于是我这样做：

1. 使用其他基于 Arch linux 的 distro (我正在用 Archlabs)
1. 买老机器
2. Linux Kernel 选择 lts 版本
3. 留意一些需要大量 python dependencies 的 package(笑)

还好我只是写代码的，代码都 git 上去 remote repo 了，所以没有啥要备份的东西，一直用到现在～

pacman 太好用了，无论是下载速度，还是管理包都很棒！每天坚持 `sudo pacman -Syyu`。另外社区没有的包，不用担心还有AUR有，基本上要啥有啥，感谢各位打包的人！
还有 Arch Wiki！ 我是看肥猫的视频才知道 Arch Wiki 上的文档都是开发者们无私贡献和自觉维护，顿时感觉开源精神太伟大了！

Linux 目前给我最大的感受是真的 free，比如回想起一个细节：这一路使用 linux 下来没有遇到要用户同意的地方，比如其他 os 经常遇到的需要同意用户协议才能继续使用。另外是随便改改改，各种 plugin，配置文件随便改(当然注意是否系统文件)，然后从折腾这些插件时学习到很多 linux command。还能透过看其他大佬是怎么弄，从而学习到一些大佬的良好代码风格和文件管理，一路上看下来网络上仿佛个个都是大佬，他们电脑都很酷，个个都是人才！

Linux 为我带来了快和敏捷，我正在用 bspwm，这些 wm 跟 emacs vim 一些理念很接近：能用键盘就坚决不用鼠标，生产力up！另外最喜欢的便是整个系统+wm 开机才占300多mb，对我这个老机器十分友好 QAQ

总的来说，一切体验良好，继续学习。
也在此感谢一直为开源付出的人们，谢谢你们让我拥有了属于“自己”的电脑！
