---
layout: post
title: How to learn code or other things
date: 2020-09-12 18:34:28
last_modified_at: 2020-09-12 18:34:28
categories: [life]
---

Follow the steps below:

1. Start extremely simple
2. Allow it to look at terrible
3. Repeat and iterate time over time
4. Have patience!
