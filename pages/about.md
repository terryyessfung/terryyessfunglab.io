---
layout: page
title: About me
permalink: /about/
---

--- 

Hi, I'm Terry Fung. My work and daily life all in Hong Kong now. 

--- 

Now most of the time, I'm a programmer. Currently studying on Web, Tcp socket, Linux.
I like Ruby, Python, Javascript(sometimes), lisp and shell script. I enjoy to think about using programming to describle 
real world object relationship. I love the code style which is pure, clean and easy for human read, 
but the code of maintainable is most I concerned.

---

I'm a hobby photographer. Please don't ask me any camera technical words like "ISO", "shutter", "3.5mm", "1/4", etc,
cus you know 'just hobby 😂'. When I was high school, I suddenly realized that every things just changed too fast and camera
can help me to capture the time as frame so I start it! Taking photo let me enjoy the world and I start to care the things 
around me even it is tiny.Like flowers under the sunset, it's a gorgeous monent :)

---

I love music. Chill, juzz, pop songs. Love Taylor swift songs🙈
![](https://ucarecdn.com/4f7dbb63-aaf3-4eb8-9030-5d7cb02a6159/-/format/auto/-/quality/lightest/)

One of my favourite lyric.

Recently, I'm looping Billie Eillish -- Halley's Comet. It's a sweet,gentle and romantic song😍.

---

Love movie,TV show.
![](https://ucarecdn.com/8fc8a549-8ffd-4538-8cf0-04e31b897185/-/format/auto/-/quality/lightest/)

Modern family, I love this show so much QAQ

![](https://ucarecdn.com/f913a84c-955e-41ac-b719-3b97a7568b46/-/format/auto/-/quality/lightest/)

Did you watch this show? 2 seasons. I think the flow, color, camera and cast all the best 
especially the bgm in this show which is so prefect! 

I was barely talking myself on the internet, so I stop here. 
Posts and gallery can explain my thought further, plz read those if you interest.

---

That me, always writing dummy code, taking silly photos, not cool follow the pop culture just a normal person :)

